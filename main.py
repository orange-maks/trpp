import sys


def convert(val, start, to):
	if start=="C":
		if to=="K":
			return float(val)+273.15
		elif to=="F":
			return float(val)*1.8+32
		else:
			return "Invalid input"
	elif start=="K":
		if to=="C":
			return float(val)-273.15
		elif to=="F":
			return convert(float(val)-273.15,"C","F")
		else:
			return "Invalid input"
	elif start=="F":
		if to=="C":
			return (float(val)-32)*0.55
		elif to=="K":
			return convert(float(val),"F","C")+273.15
		else:
			return "Invalid input"
	else:
		return "Invalid input"


if __name__ == '__main__':
	print(convert(sys.argv[1],sys.argv[2],sys.argv[3]))	
