from main import convert

def test_C_conv_to_K():
	assert round(convert(0,"C","K"),2) == 273.15

def test_K_conv_to_C():
	assert round(convert(0,"K","C"),2) == -273.15

def test_F_conv_toK():
	assert round(convert(0,"F","K"),2) == 255.55

def test_error():
	assert convert(0,"far","C") == "Invalid input"
